##**BSDPy Docker container**

###**Common setup notes**

The BSDPy container must run with ports 67/udp (bootps), 69/udp (tftpd) and 80 (httpd) bound to all active interfaces on the host, using `-p 0.0.0.0:port:port/proto`.

When running a BSDPy Docker container using boot2docker a bridged VirtualBox adapter must be present with an IP on the network to which target NetBoot clients connect in order to successfully serve BSDP, TFTP and HTTP requests.

Use the `-e` flag to pass the IP of the bridged interface into the container via the **DOCKER_BSDPY_IP** environment variable. In the container *bsdpserver.py* will detect the presence of `$DOCKER_BSDPY_IP` and use the IP to serve TFTP and HTTP requests.

When using boot2docker in a basic setup that will usually be eth1 but can be any other non-NAT interface that has been configured properly for your network environment. To look up the IP of the desired bridged interface:

    my-mac$ boot2docker ssh
    docker@localhost's password:
    # We want to use eth1 as our BSDPy public interface
    docker@boot2docker:~$ ip addr show eth1 | grep inet[^6]
        inet x.x.x.x/24 brd x.x.x.255 scope global eth1

For Linux hosts running the BSDPy container directly the same `$DOCKER_BSDPY_IP` environment variable can be used to configure BSDPy to serve NetBoot from a specific IP address.

TFTP root must be `chmod -R 777 /nbi` and `chown -R root:wheel /nbi` - otherwise TFTP will fail with permission errors and NetBoot will fail.

###**Runtime notes**

###Manual/interactive run

    host$ docker run -i -t -p 0.0.0.0:69:69/udp -p 0.0.0.0:67:67/udp -p 0.0.0.0:80:80 --entrypoint /usr/bin/env -e [DOCKER_BSDPY_IP=<$EXTERNAL_IP>] --name bsdpy_bash bruienne/bsdpy bash

###Daemonized run

    host$ docker run -d -p 0.0.0.0:69:69/udp -p 0.0.0.0:67:67/udp -p 0.0.0.0:80:80 -e [DOCKER_BSDPY_IP=<$EXTERNAL_IP>] --name bsdpy_daemon bruienne/bsdpy

###View runtime logs

The BSDPy image is setup to output its log in /var/log/bsdpserver.py to STDOUT. This mean you can monitor the output using `docker logs -f bsdpy_daemon`. This will behave the same way as tail -f (follow) does. Running docker logs without -f will act like tail does without the -f flag and output the 10 most recent entries.

###NetBoot image (NBI) storage

Since the container will not have any NetBoot images populating /nbi by default one can use a tool like scp to do so (run `docker commit <container ID>` to permanently add them to a newly layered image) or by mounting a read-only directory from the host using `-v /path/on/host/nbi:/nbi:ro`.

###To permanently add NBI(s) to the BSDPy image:

    # Run a base BSDPy container using --rm so it's removed later on
    host$ CONTAINER_ID=$(docker run -i -t --rm --entrypoint /usr/bin/env bruienne/bsdpy bash)
    # Inside container copy over one or more NBIs
    docker$ scp -r user@server:/path/to/MyImage.nbi /nbi
    docker$ chmod -R 777 /nbi
    docker$ chown -R root:wheel /nbi
    # On the host commit the running container with added NBI(s) to a new image
    host$ NEW_IMAGE=$(docker commit $CONTAINER_ID)
    # Stop the running container, it will be removed
    host$ docker stop $CONTAINER_ID
    # Now run the new image as a daemon with the full set of options (ports, etc.)
    host$ docker run -d (...) $NEW_IMAGE

###To mount a directory from the host containing NBI(s):

    # Mount a host-based directory with NBI(s) directly to the container
    host$ docker run -d (...) -v /host/path/NBI:/nbi:rw --name bsdpy_daemon bruienne/bsdpy