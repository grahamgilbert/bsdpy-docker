# Simplified Dockerfile for BSDPy v0.2
FROM bruienne/bsdpy-docker

ENV DOCKER_BSDPY_IFACE eth0
ENV DOCKER_BSDPY_PROTO http
ENV DOCKER_BSDPY_PATH /data/bsdpy
RUN chmod 755 /start.sh
ENTRYPOINT ["/start.sh"]
